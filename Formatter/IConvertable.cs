﻿/*
 * Created by SharpDevelop.
 * User: Нармина
 * Date: 25.06.2018
 * Time: 22:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace Formatter
{
	public interface IConvertable<T> where T:class
	{
		string ConvertToXml(List<T>items);
		string ConvertToJson(List<T>items);	
	}
}
