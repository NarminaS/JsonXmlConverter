﻿/*
 * Created by SharpDevelop.
 * User: Нармина
 * Date: 25.06.2018
 * Time: 22:28
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace Formatter
{
	class Program
	{
        public static void Main(string[] args)
        {
            List<Person> people = new List<Person>()
            {
                 new Person() { Id = 1, Age=10, Name="Person 1" },
                 new Person() { Id = 2, Age=11, Name="Person 2" },
                 new Person() { Id = 3, Age=12, Name="Person 3" },
                 new Person() { Id = 4, Age=13, Name="Person 4" },
                 new Person() { Id = 5, Age=14, Name="Person 5" }
            };

            Converter<Person> c = new Converter<Person>();

            Console.WriteLine(c.ConvertToXml(people));
            Console.WriteLine();
            Console.WriteLine(c.ConvertToJson(people));

			Console.ReadKey(true);
		}
	}
}