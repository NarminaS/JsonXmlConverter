﻿/*
 * Created by SharpDevelop.
 * User: Нармина
 * Date: 25.06.2018
 * Time: 22:33
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using System.Reflection;
using System.Collections.Generic;

namespace Formatter
{
	public class Converter<T>:IConvertable<T> where T:class 
	{
		public string ConvertToXml(List<T> items)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("<"+nameof(items)+">");
            foreach (T item in items)
            {
                sb.Append("\n\n\r\t" + "<" + item.GetType().Name + ">" );
                foreach (PropertyInfo pi in item.GetType().GetProperties())
                {
                    sb.Append("\n\r\t\t"+"<"+pi.Name+">" + pi.GetValue(item) + "</" + pi.Name + ">");
                }
                sb.Append("\n\r\t</" + item.GetType().Name + ">");
            }
            sb.Append("\n\r</" + nameof(items) + ">");
            return sb.ToString();
		}

        public string ConvertToJson(List<T> items)
		{
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            foreach (T item in items)
            {
                sb.Append("\n\n\r\t" + "{");
                foreach (PropertyInfo pi in item.GetType().GetProperties())
                {
                    string name = pi.Name;
                    string value = pi.GetValue(item).ToString();
                    sb.Append($" \"{name}\":\"{value}\" " );
                }
                sb.Append("}");
            }
            sb.Append("\n\r]");
            return sb.ToString();
        }
		
	}
}
