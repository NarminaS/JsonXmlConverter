﻿/*
 * Created by SharpDevelop.
 * User: Нармина
 * Date: 25.06.2018
 * Time: 22:30
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace Formatter
{
	public class Person
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int Age { get; set; }	
	}
}
